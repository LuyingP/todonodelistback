const listModel = require("../db/listShema");


const creatList= async function(req,res){

   try{  
        list = new listModel({
          userid:req.params.userid,
          titre: req.body.titre,
          taches: req.body.taches
          })     
        await list.save();
        res.status(200).send(list);
      }
      
      catch (err) {
          console.log(err.message)
        res.send(err.message);
        }
}


const findListByTitle = (req, res) => {
    const titreSaisi = req.params.titre;
    listModel.find({ titre: titreSaisi})
      .then(data => {
        if (!data)
          res.status(404).send({ message: "Not found list with title "});
        else {
          if (data.length == 0) {
            res.send("no list found")
          } else {
            res.send(data)
            return data
          }
        }
  
      })
      .catch(err => {
        res
          .status(500)
          .send(err.message);
      });
  }


  const findListByUser= (req,res)=>{

    const useridpar=req.params.userid
    listModel.find({userid:useridpar})
      .then(data => {
        if (!data)
          res.status(404).send({ message: "Not found list with userid "}+useridpar);
        else {
            if (data.length == 0) {
                res.send("no list found by userid"+useridpar)
              } else {
            res.send(data)
            // console.log(data instanceof Array)
            return data
              }
        }
  
      })
      .catch(err => {
        res
          .status(500)
          .send(err.message);
        console.log(err.message)
      });

  }


const modifierList=async (req,res)=>{

     const titreSaisi=req.params.titre
  
    listModel.updateOne({ titre: titreSaisi},
    { $push: { "taches": [req.body] } },
    function(err, result) {
      if (err) {
          console.log(err)
        res.send(err);
      } else {
        res.send(result);
      }
    }
  );
     

}


  


exports.creatList = creatList
exports.findListByTitle=findListByTitle
exports.findListByUser=findListByUser
exports.modifierList=modifierList