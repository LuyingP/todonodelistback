const mongoose=require("mongoose");
var listSchema=mongoose.Schema({
    userid:String,
    titre:String,
    taches:[
        {
            titre_tache: {type:String, required:true},
            description: String,
            date_creation_date: {type:Date, default:Date.now},
            date_creation_string: {type:String,default:new Date().toLocaleDateString()},
            date_fin: {type:String, required: true}
        }
    ]
})

var listModel=mongoose.model("List",listSchema);

module.exports=listModel;
