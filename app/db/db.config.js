const mongoose = require("mongoose");

const dbUrl = "mongodb://localhost:27017/todolist";
const dbUrltest="mongodb://mongo/todolist"


function connect() {
    return new Promise((resolve, reject) => {
        if (process.env.NODE_ENV === 'test') {
                    mongoose.connect(dbUrltest, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
                        .then((res, err) => {
                            if (err) return reject(err);

                            else {
                                resolve();
                                console.log("MongoDB test connection successful");
                            }
                        })
        } else {

            mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })

                .then((res, err) => {
                    if (err) return reject(err);
                    else {
                        resolve();
                        console.log("MongoDB connection successful");
                    }
                })

        }
    })
}

function close() {
    console.log("mongodb disconnect");
    return mongoose.disconnect();
}

module.exports = { connect, close };
