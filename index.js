const express = require("express")
const app=express();
const bodyParser = require("body-parser")
const db = require("./app/db/db.config")
const listService=require("./app/services/listService")
const cors = require("cors");
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200");
   res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
  next();
});

  


app.post("/mylist/:userid",listService.creatList)
app.get("/mylist/:titre",listService.findListByTitle)
app.get("/mylist/:userid/lists",listService.findListByUser)
app.put("/mylist/:titre",listService.modifierList)



db.connect()
  .then(() => {
    app.listen(4545,()=>{
      console.log(`server is running at 3000`);
  });
   
  });
  module.exports=app;